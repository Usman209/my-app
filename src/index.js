import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';

const pizzaData = [
	{
		name: 'pizza1',
		ingredients: 'chilli,cheese',
		price: 18,
		photoName: 'pizzaImage.jpg',
		soldOut: false
	},
	{
		name: 'pizza2',
		ingredients: 'chilli,cheese',
		price: 18,
		photoName: 'pizzaImage.jpg',
		soldOut: false
	},
	{
		name: 'pizza3',
		ingredients: 'chilli,cheese',
		price: 18,
		photoName: 'pizzaImage.jpg',
		soldOut: false
	},
	{
		name: 'pizza4',
		ingredients: 'chilli,cheese',
		price: 18,
		photoName: 'pizzaImage.jpg',
		soldOut: false
	},
	{
		name: 'pizza5',
		ingredients: 'chilli,cheese',
		price: 18,
		photoName: 'pizzaImage.jpg',
		soldOut: true
	}
];

function App() {
	return (
		<div className="container">
			<Header />
			<Menu />
			<Footer />
		</div>
	);
}

// Rendering Lists
function Pizza(props) {
	return (
		// Setting Classes and Text Conditionally
		<li className={`pizza ${props.pizzaObj.soldOut ? 'sold-out' : ''}`}>
			<img src={props.pizzaObj.photoName} alt="pizza image" />
			<div>
				<h3>{props.pizzaObj.name} </h3>
				<p>{props.pizzaObj.ingredients}</p>
				<span>{props.pizzaObj.soldOut ? 'sold out ' : props.pizzaObj.price} </span>
			</div>
		</li>
	);
}

function Header() {
	// inline css apply
	// const style={color:'red',fontSize:'48px',textTransform:'upperCase'}
	const style = {};
	return (
		<header className="header">
			<h1 style={style}>Fast React Pizza </h1>
		</header>
	);
}
function Menu() {
	const numPizzas = pizzaData.length;

	return (
		// sending data with props
		<main className="menu">
			<h2>Our Menu</h2>

			{numPizzas > 0 ? (
				<ul className="pizzas">{pizzaData.map((pizza) => <Pizza pizzaObj={pizza} key={pizza.name} />)}</ul>
			) : (
				<p>we are still working come back later </p>
			)}
		</main>
	);
}
function Footer() {
	const hour = new Date().getHours();
	const openHour = 1;
	const closeHour = 24;
	const isOpen = hour >= openHour && hour <= closeHour;
	console.log(isOpen);

	return (
		// Conditional Rendering With &&

		<footer className="footer">
			{isOpen && (
				<div className="order">
					<p> we're currently open until {closeHour}:00 </p>
					<button className="btn">Order</button>
				</div>
			)}
		</footer>
	);
	// return React.createElement('footer', null, "we're currently open !")
}

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(<App />);
